module.exports = {
    HextoRGB: (redHex, greenHex, blueHex) => {
        const red = parseInt(redHex, 16); 
        const green = parseInt(greenHex, 16);
        const blue = parseInt(blueHex, 16);
        return `${red}, ${green}, ${blue}`; 
    }
}