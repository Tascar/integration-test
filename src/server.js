const express = require("express");
const converter = require("./converter");

const app = express();
const port = 3000

app.get('/', (req, res) => res.send("Hello"));

app.get('/hex-to-rgb', (req, res) => {
    const redHex = req.query.redHex;
    const greenHex = req.query.greenHex;
    const blueHex = req.query.blueHex;
    const rgb = converter.HextoRGB(redHex, greenHex, blueHex);
    res.send(rgb.toString());
})
if (process.env.NODE_ENV === 'test') {
    module.exports = app;
} else {
app.listen(port, () => console.log(`Server: localhost:${port}`));
}