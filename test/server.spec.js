// Integration test
const expect = require("chai").expect;
const request = require("request");
const app = require("../src/server");
const port = 3000;

describe("Color Code Converter API", () => {

    before("Start server before runnning tests", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening to localhost:${port}`);
            done();
        });
    });

    describe("Hex to RGB conversion", () => {
        const url = `http://localhost:${port}/hex-to-rgb?redHex=ff&greenHex=ff&blueHex=ff`;

        it("returns status 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("returns the color in rgb", (done) => {
            request(url, (error, response, body) => {
                expect(body).to.equal("255, 255, 255");
                done();
            });
        });
    });

    after("Stop server after tests", (done) => {
        server.close();
        done();
    });
});