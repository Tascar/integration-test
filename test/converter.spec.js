// TDD - Unit Testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("HEX to RGB conversion", () => {
        it("converts the basic colors", () => {
            const red = converter.HextoRGB('ff', '00', '00'); // red 255, 0, 0
            const green = converter.HextoRGB('00','ff', '00'); // green 0, 255, 0
            const blue = converter.HextoRGB('00', '00', 'ff'); // blue 0, 0, 255

            expect(red).to.equal("255, 0, 0");
            expect(green).to.equal("0, 255, 0");
            expect(blue).to.equal("0, 0, 255");
        });
    });
});